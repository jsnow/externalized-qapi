# -*- Mode: Python -*-

# Historical schema addendum for QEMU v1.6.0
# Compiled lovingly and painstakingly by hand.
#
# This file contains definitions that were not QAPIfied at the time of
# release *and* whose accurate definitions cannot be inferred from the
# qmp-commands.hx file.
#
# Remaining definitions are generally pulled from the first version in
# which they were QAPIfied, and then ported backwards when
# possible/appropriate, modifying fields, names, types etc. to
# correspond to the actual protocol as defined in this version.
#
# For any definition added here, a comment is added that explains if the
# definition is either unchanged or modified prior to its formal
# QAPIfication in whichever version that occurred in.
#
# This is only for auditing and review purposes.

# == Events ==

# Events were found by grepping for QEVENT_ and checking callsites to
# determine possible arguments. They are defined in monitor.h;
# typedef enum MonitorEvent { ... } MonitorEvent;
#
# callsites are monitor_protocol_event(QEVENT_FOO, data);

# Note that events were not qapified until v2.1.0.

# Unchanged by v2.1.0
{ 'event': 'SHUTDOWN' }

# Unchanged by v2.1.0
{ 'event': 'RESET' }

# Unchanged by v2.1.0
{ 'event': 'POWERDOWN' }

# Unchanged by v2.1.0
{ 'event': 'STOP' }

# Unchanged by v2.1.0
{ 'event': 'RESUME' }

# Unchanged by v2.1.0
{
    'enum': 'NetworkAddressFamily',
    'data': [
        'ipv4',
        'ipv6',
        'unix',
        'unknown'
    ]
}

# Unchanged by v2.1.0
{
    'struct': 'VncBasicInfo',
    'data': {
        'host': 'str',
        'service': 'str',
        'family': 'NetworkAddressFamily'
    }
}

# Unchanged by v2.1.0
{
    'struct': 'VncServerInfo',
    'base': 'VncBasicInfo',
    'data': {
        '*auth': 'str'
    }
}

# Unchanged by v2.1.0
{
    'event': 'VNC_CONNECTED',
    'data': {
        'server': 'VncServerInfo',
        'client': 'VncBasicInfo'
    }
}

# Unchanged by v2.1.0
{
    'event': 'VNC_INITIALIZED',
    'data': {
        'server': 'VncServerInfo',
        'client': 'VncClientInfo'
    }
}

# Unchanged by v2.1.0
{
    'event': 'VNC_DISCONNECTED',
    'data': {
        'server': 'VncServerInfo',
        'client': 'VncClientInfo'
    }
}

# Unchanged by v2.1.0
{
    'enum': 'IoOperationType',
    'data': [
        'read',
        'write'
    ]
}

# Unchanged by v2.1.0
{
    'enum': 'BlockErrorAction',
    'data': [
        'ignore',
        'report',
        'stop'
    ]
}

# Unchanged by v2.1.0
{
    'event': 'BLOCK_IO_ERROR',
    'data': {
        'device': 'str',
        'operation': 'IoOperationType',
        'action': 'BlockErrorAction'
    }
}

# Unchanged by v2.1.0
{
    'event': 'RTC_CHANGE',
    'data': {
        'offset': 'int'
    }
}

# Unchanged by v2.1.0
{
    'enum': 'WatchdogExpirationAction',
    'data': [
        'reset',
        'shutdown',
        'poweroff',
        'pause',
        'debug',
        'none'
    ]
}

# Unchanged by v2.1.0
{
    'event': 'WATCHDOG',
    'data': {
        'action': 'WatchdogExpirationAction'
    }
}

# Unchanged by v2.1.0
{
    'struct': 'SpiceBasicInfo',
    'data': {
        'host': 'str',
        'port': 'str',
        'family': 'NetworkAddressFamily'
    }
}

# Unchanged by v2.1.0
{
    'struct': 'SpiceServerInfo',
    'base': 'SpiceBasicInfo',
    'data': {
        '*auth': 'str'
    }
}

# Unchanged by v2.1.0
{
    'event': 'SPICE_CONNECTED',
    'data': {
        'server': 'SpiceBasicInfo',
        'client': 'SpiceBasicInfo'
    }
}

# Unchanged by v2.1.0
{
    'event': 'SPICE_INITIALIZED',
    'data': {
        'server': 'SpiceServerInfo',
        'client': 'SpiceChannel'
    }
}

# Unchanged by v2.1.0
{
    'event': 'SPICE_DISCONNECTED',
    'data': {
        'server': 'SpiceBasicInfo',
        'client': 'SpiceBasicInfo'
    }
}

# Unchanged by v1.7.0
{
    'enum': 'BlockJobType',
    'data': [
        'commit',
        'mirror',
        'stream',
        'backup'
    ]
}

# Unchanged by v2.1.0
{
    'event': 'BLOCK_JOB_COMPLETED',
    'data': {
        'type'  : 'BlockJobType',
        'device': 'str',
        'len'   : 'int',
        'offset': 'int',
        'speed' : 'int',
        '*error': 'str'
    }
}

# Unchanged by v2.1.0
{
    'event': 'BLOCK_JOB_CANCELLED',
    'data': {
        'type'  : 'BlockJobType',
        'device': 'str',
        'len'   : 'int',
        'offset': 'int',
        'speed' : 'int'
    }
}

# Unchanged by v2.1.0
{
    'event': 'BLOCK_JOB_ERROR',
    'data': {
        'device'   : 'str',
        'operation': 'IoOperationType',
        'action'   : 'BlockErrorAction'
    }
}

# Unchanged by v2.1.0
{
    'event': 'BLOCK_JOB_READY',
    'data': {
        'type'  : 'BlockJobType',
        'device': 'str',
        'len'   : 'int',
        'offset': 'int',
        'speed' : 'int'
    }
}

# Unchanged by v2.1.0
{
    'event': 'DEVICE_DELETED',
    'data': {
        '*device': 'str',
        'path': 'str'
    }
}

# Unchanged by v2.1.0
{
    'event': 'DEVICE_TRAY_MOVED',
    'data': {
        'device': 'str',
        'tray-open': 'bool'
    }
}

# Unchanged by v2.1.0
{ 'event': 'SUSPEND' }

# Unchanged by v2.1.0
{ 'event': 'SUSPEND_DISK' }

# Unchanged by v2.1.0
{ 'event': 'WAKEUP' }

# Unchanged by v2.1.0
{
    'event': 'BALLOON_CHANGE',
    'data': {
        'actual': 'int'
    }
}

# Unchanged by v2.1.0
{ 'event': 'SPICE_MIGRATE_COMPLETED' }

# Unchanged by v2.1.0
{
    'enum': 'GuestPanicAction',
    'data': [
        'pause'
    ]
}

# Unchanged by v2.1.0
{
    'event': 'GUEST_PANICKED',
    'data': {
        'action': 'GuestPanicAction'
    }
}

# == Commands ==

# QMP commands in this version are defined between qapi-schema.json and
# qmp-commands.hx.
#
# This version has no commands that aren't QAPIfied or otherwise handled
# adequately by the hxtool parser.

# Commands defined in the schema that do not use the autogenerated
# marshaller:
# - qom-set
# - qom-get
# - netdev_add

# Unqapified commands:
# - device_add (v2.8.0)
# - client_migrate_info (v2.4.0)

# device_add: handled via hardcoded override in qapi/hxtool.py
