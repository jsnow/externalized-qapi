import importlib.resources
import json
import os
from pathlib import Path
from subprocess import run
from typing import Optional, Union

from qapi.compile import compile_schema
from qapi.schema import QAPISchema, QAPISchemaDefinition, QAPISchemaCommand, QAPISchemaEvent


def qapi_compile(root: Optional[Union[str, Path]] = None):
    resource_root = importlib.resources.files('qapi')

    if root is None:
        root = Path(os.getcwd())
    else:
        root = Path(root)

    os.chdir(root)
    print(f"QEMU.git root path: {str(root)}")

    # This is a little hokey and doesn't necessarily get the right tag
    # if you have multiple defined for a particular release commit, but
    # after I deleted a few, "worksforme"
    ret = run(["git", "describe", "--tags"], capture_output=True)
    str_version = ret.stdout.decode().strip()
    assert str_version.startswith("v")
    print(f"QEMU version: {str_version}")

    path = root.joinpath("qapi-schema.json")
    if not path.exists():
        path = root.joinpath("qapi/qapi-schema.json")
        if not path.exists():
            path = None

    hxpath = root.joinpath("qmp-commands.hx")
    if not hxpath.exists():
        hxpath = root.joinpath("qemu-monitor.hx")
        if not hxpath.exists():
            hxpath = None

    if path:
        print(f"QAPI Schema: {str(path)}")
    else:
        print("QAPI Schema: -- none detected --")

    if hxpath:
        print(f"QMP .HX file: {str(hxpath)}")
    else:
        print("QMP .HX file: -- none detected --")

    if not (path or hxpath):
        raise Exception(
            "Couldn't locate [qapi/]qapi-schema.json nor "
            "qmp-commands.hx or qemu-monitor.hx. "
            "Please run this command from the qemu.git root."
        )

    files = [path, hxpath]
    files = [f for f in files if f is not None]

    # Resource files are weird, because they might not be real files;
    # so I have to conjure it into being extant with importlib,
    # and keep it "alive" until QAPISchema is done with it:
    resource = resource_root.joinpath(f"schemata/{str_version}.json")
    with importlib.resources.as_file(resource) as addendum_file:
        if addendum_file.exists():
            print(f"QAPI addendum file: {str(addendum_file)}")
            files.append(str(addendum_file))
        else:
            print(f"QAPI addendum file: -- none detected --")

        print("\n  --  \n")

        print("Parsing schema ...")
        schema = QAPISchema(*files)
        print("Schema parsed OK!")

    print("\n  --  \n")

    commands = [ent for ent in schema._entity_list if isinstance(ent, QAPISchemaCommand)]
    events = [ent for ent in schema._entity_list if isinstance(ent, QAPISchemaEvent)]
    defns = [ent for ent in schema._entity_list if isinstance(ent, QAPISchemaDefinition)]
    print(f"Commands: {len(commands)}")
    print(f"Events: {len(events)}")
    print(f"Definitions: {len(defns)}")

    print("Compiling schema ...")
    json_schema = compile_schema(schema, str_version)
    print("Schema compiled OK!")

    print("\n  --  \n")

    outname = f"qapi-compiled-{str_version}.json"
    print(f"Writing compiled schema to {outname!r}")
    with open(outname, "w") as file:
        json.dump(json_schema, file, indent=2)
    print(f"Compiled schema written successfully to {outname!r}")


def main():
    qapi_compile()


if __name__ == "__main__":
    main()
