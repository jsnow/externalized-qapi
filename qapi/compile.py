from typing import Dict, Optional, Set, List

from qapi.schema import (
    QAPISchema,
    QAPISchemaEntity,
    QAPISchemaType,
    QAPISchemaArrayType,
    QAPISchemaBuiltinType,
    QAPISchemaCommand,
    QAPISchemaEnumType,
    QAPISchemaEvent,
    QAPISchemaObjectType,
    QAPISchemaAlternateType,
)


# Known shortcomings:
#
# - If a cyclic reference is found beneath an alternate or branch, the
#   reference back to the schema that defines the recursive type will
#   have a numerical index. (i.e. altname/0) - these references may not
#   be stable across versions.
#
#   The only way to fix this shortcoming would be to acknowledge (or
#   generate) a factored type name; which I have otherwise avoided in
#   this prototype as an explicit goal. We can loosen that constraint if
#   so desired.
#
#   The only real downside to allowing numerical indices is that if a
#   cycle below this point occurs (and points to somewhere also below
#   this point, i.e. the entire cycle is contained below the union), the
#   literal text of the reference may not be stable - a diff tool will
#   have extra work to do to understand if the nature of the cycle has
#   changed or not and won't be able to just compare the pointer
#   content.


# Mapping between QAPI's json_type() and the primitive type json-schema expects.
json_schema_types = {
    "boolean": "boolean",
    "int": "integer",
    "number": "number",
    "string": "string",
    "null": "null",
}


def compile_type(
    qtype: QAPISchemaType,
    path: str,
    types_seen: Optional[Dict[str, str]] = None,
) -> Dict[str, object]:
    """Compile a QAPI Schema type into a JSON-Schema object.

    :param qtype: The QAPI type to compile into a JSON Schema
    :param path: The JSON Pointer path that serves as a reference to the
        object being compiled. This is used for reference generation
        when needed for the purposes of cycle avoidance.
    :param types_seen: A collection of QAPI type names seen so far,
        mapped to their JSON Pointer paths. This is used for cycle
        detection and avoidance.

    Generally, returns a dict (modeling a JSON object) with the following keys:
      - $ref: This is a JSON Pointer reference to avoid infinite recursion.
            No other keys are present in this case.
      - type: one of 'object', 'array', 'string', 'boolean', 'integer',
            'number'. If this key is absent, the type is unconstrained
            ("any type").

    When qtype is an Enum:
      - type: This key's value will be "string".
      - enum: This is an array of possible enumeration values.

    When qtype is an Array:
      - type: This key's value will be "array".
      - items: This key contains a subschema that defines the element type.

    When qtype is an Object:
      - type: This key's value will be "object".
      - properties: This key contains an object whose keys are object
            field names and whose values are subschema validating those
            fields.
      - required: This key contains an array of strings that specify
            which fields are non-optional. It may be absent if *all*
            object properties are optional.
      - x-discriminator: when the object contains branches, this field
            contains the name of the discriminator field. The name will
            always also appear in the required array.
      - oneOf: When the object contains branches, this field contains an
            array of subschema; each of which validates the properties
            unique to each branch. Each subschema in this array places a
            constraint on the literal value of the discriminator to
            achieve the discriminated union semantics.

    When qtype is an Alternate:
      - oneOf: This key provides an array of possible subschema. No
            type key will be present. Each subschema will have an
            'x-qemu-branch-name' key that names the branch, used only
            for diff output readability.
    """
    # No type specified, default is an empty object.
    if not qtype:
        return {"type": "object"}

    if types_seen is None:
        types_seen = {}
    else:
        # Make a copy so each recursive path is fully isolated.
        types_seen = dict(types_seen)

    # Allow arrays to produce before the cycle check; we want to stub
    # out the recursive element type with a reference, not the array
    # itself.
    if isinstance(qtype, QAPISchemaArrayType):
        return {
            "type": "array",
            "items": compile_type(
                qtype.element_type, f"{path}/items", types_seen
            ),
        }

    if qtype.name in types_seen:
        # Avoid infinite recursion, use a reference.
        return {
            "$ref": types_seen[qtype.name],
        }

    types_seen[qtype.name] = path

    if isinstance(qtype, QAPISchemaAlternateType):
        subschema = []
        for i, alt in enumerate(qtype.alternatives.variants):
            subschema.append(
                compile_type(
                    alt.type,
                    f"{path}/oneOf/{i}",
                    types_seen,
                )
            )
            subschema[-1]["x-qemu-branch-name"] = alt.name
        return {"oneOf": subschema}

    if isinstance(qtype, QAPISchemaEnumType):
        return {
            "type": "string",
            "enum": sorted([e.name for e in qtype.members]),
        }

    if isinstance(qtype, QAPISchemaBuiltinType):
        if qtype.json_type() == "value":
            # "any" type; JSON-Schema represents this with an empty
            # object with no 'type' key.
            return {}
        return {"type": json_schema_types[qtype.json_type()]}

    assert isinstance(qtype, QAPISchemaObjectType), type(qtype)
    obj = {
        "type": "object",
        "properties": {},
    }

    required = set()
    for memb in qtype.members:
        if not memb.optional:
            required.add(memb.name)
        obj["properties"][memb.name] = compile_type(
            memb.type, f"{path}/properties/{memb.name}", types_seen
        )

    # Oh bother.
    if qtype.branches:
        tag_member = qtype.branches.tag_member
        assert tag_member.name in required
        # JSON-Schema does not offer a discriminator feature.
        #
        # OpenAPI offers a non-standard "discriminator" keyword that
        # allows you to name the discriminator, which is used only as a
        # "hint" for a validator and to give better error messages when
        # the oneOf condition fails. I borrow the concept here, but use
        # the "x-" prefix to specifically carve this out as a schema
        # extension. It has no impact on a standard validator.
        #
        # What we do for the standard behavior is create an "anonymous
        # union" where each branch explicitly specifies the field and
        # value of the discriminator it takes.
        obj["x-discriminator"] = tag_member.name
        branches = []

        for i, variant in enumerate(qtype.branches.variants):
            subschema = compile_type(
                variant.type,
                f"{path}/oneOf/{i}",
                types_seen,
            )

            # I think branches always have to be objects, ...right?
            assert 'properties' in subschema

            # Add the discriminator constraint into the branch subschema:
            subschema['properties'][tag_member.name] = {
                'const': variant.name,
            }

            # And make it required.
            subschema.setdefault('required', []).append(tag_member.name)

            branches.append(subschema)
        obj['oneOf'] = branches

    obj["properties"] = dict(sorted(obj["properties"].items()))
    if required:
        obj["required"] = sorted(list(required))
    return obj


def compile_features(ent: QAPISchemaEntity) -> List[Dict[str, object]]:
    """
    Gather, normalize and sort the list of features for an entity.
    """
    features = []
    for feat in ent.features:
        f = {"name": feat.name}
        if feat.ifcond.is_present():
            f["if"] = feat.ifcond.ifcond
        features.append(f)

    # Commands have some "meta-features" that do matter for API
    # stability concerns; document them alongside the traditional
    # features list.
    if isinstance(ent, QAPISchemaCommand):
        if ent.allow_oob:
            features.append({"name": "allow-oob"})
        if not ent.success_response:
            features.append({"name": "no-success-response"})
        if ent.allow_preconfig:
            features.append({"name": "allow-preconfig"})

    features = sorted(features, key=lambda f: f["name"])
    return features


def compile_schema(schema: QAPISchema, version: str) -> Dict[str, object]:
    commands = []
    events = []

    for ent in schema._entity_list:
        if isinstance(ent, QAPISchemaCommand):
            commands.append(ent)
        if isinstance(ent, QAPISchemaEvent):
            events.append(ent)

    commands = sorted(commands, key=lambda c: c.name)
    events = sorted(events, key=lambda e: e.name)

    def _names(entlist):
        return [ent.name for ent in entlist]

    def _try_int(value):
        try:
            return int(value)
        except ValueError:
            return value

    if version.startswith("v"):
        version = version[1:]
    version_components = [_try_int(x) for x in version.split(".")]

    # Using "x-qemu-" prefix for QAPI metadata that is not meant to be
    # JSON-Schema semantic data; i.e. these keys have no particular
    # meaning to JSON-Schema; they are "annotations".
    #
    # The json-schema blog states that the next JSON-Schema draft
    # will specify the "x-" prefix for arbitrary user annotation
    # data, and that it is backwards compatible (since unknown keys
    # are already ignored / treated as annotations by current
    # drafts.)
    #
    # It's just a place for us to stash metadata that has no meaning to
    # JSON-Schema, but is useful to us.

    output = {
        "$schema": "https://json-schema.org/draft/2020-12/schema",
        # This is a fake URL, I'm not sure if it's required to be a real one ...
        "$id": f"https://qemu.org/qapi/v{version}.json",
        "title": f"Compiled QAPI Schema for v{version}",
        "x-qemu-version": version_components,
        "x-qemu-commands": {
            "type": "string",
            "enum": _names(commands),
        },
        "x-qemu-events": {
            "type": "string",
            "enum": _names(events),
        },
        "$defs": {
            # command and event definitions go here.
        },
        # If we wanted to validate a particular JSON document as valid,
        # that subschema would go here. This document, however, is used
        # for collecting subschema in $defs and doesn't actually
        # validate anything in and of itself.
        #
        # For example: validating a valid command execution statement
        # might look something kind of like:
        # "type": "object",
        # "properties": {
        #     "oneOf": [
        #         {
        #             "properties": {
        #                 "id": { },
        #                 "execute": { "const": "cont" },
        #                 "arguments": { "$ref": "#/$defs/cont/x-qemu-arguments" }
        #             },
        #             "required": [ "execute" ]
        #         },
        #         ... repeat for each valid command ...
        #     ]
        # }
    }

    for command in commands:
        defn = {
            "x-qemu-meta": "command",
            "x-qemu-name": command.name,
            "x-qemu-features": compile_features(command),
            "x-qemu-if": command.ifcond.ifcond,
        }
        defn["x-qemu-returns"] = compile_type(
            command.ret_type,
            f"#/$defs/{command.name}/x-qemu-returns",
        )
        defn["x-qemu-arguments"] = compile_type(
            command.arg_type,
            f"#/$defs/{command.name}/x-qemu-arguments",
        )

        # Delete extraneous keys
        defn = {k: v for k, v in defn.items() if v}

        output["$defs"][command.name] = defn

    for event in events:
        defn = {
            "x-qemu-meta": "event",
            "x-qemu-name": event.name,
            "x-qemu-features": compile_features(event),
            "x-qemu-if": event.ifcond.ifcond,
        }
        defn["x-qemu-members"] = compile_type(
            event.arg_type, f"#/defs/{event.name}/x-qemu-members"
        )

        # Delete extraneous keys
        defn = {k: v for k, v in defn.items() if v}

        output["$defs"][event.name] = defn

    output["$defs"] = dict(sorted(output["$defs"].items()))
    return output
