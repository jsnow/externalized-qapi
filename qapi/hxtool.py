import json
import sys

from .parser import QAPIExpression
from .source import QAPISourceInfo

arg_type_map = {
    "F": "str",         # filename
    "B": "str",         # block device
    "s": "str",         # generic string
    "i": "int",         # 32bit
    "l": "int",         # 64bit
    "M": "int",         # unsigned; leftshifted 20x (i.e. 50M for 52428800)
    "o": "int",         # unsigned / size
    "f": "number",      # double
    "T": "number",      # double
    "b": "bool",        # originally a string for on/off. parsed as bool.

    # These arguments are generally for partial qapification.
    # We generate a definition anyway, and trust either that QAPISchema
    # won't need to use this definition (because it already has one), or
    # that we'll override the generated definition explicitly
    # (device_add)
    "O": "any",         # qemu_opts
    "q": "any",         # qapi type - should be defined in qapi-schema.json.
}

def parse_hxfile(filename):
    file = open(filename, "r")
    in_docs = False
    in_texi = False
    ifcond = None
    defns = []
    defn = None
    field = None

    for line in file:
        line = line.rstrip()
        if line.startswith("HXCOMM"):
            continue
        if line == "SQMP" and not (in_docs or in_texi):
            in_docs = True
        if line == "EQMP" and in_docs:
            in_docs = False
            continue
        if line == "STEXI" and not (in_docs or in_texi):
            in_texi = True
        if line == "ETEXI" and in_texi:
            in_texi = False
            continue

        if in_docs or in_texi:
            continue

        sline = line.strip()
        if not sline:
            continue

        if sline.startswith('#if'):
            ifcond = sline[4:]
        elif sline == "#endif":
            assert ifcond
            defns[-1]['if'] = ifcond
            ifcond = None
        elif sline == "{":
            # Starts a new definition
            defn = {}
        elif sline in ("}", "},"):
            # Ends a definition
            defns.append(defn)
            defn = None
        elif field:
            # previous defn line didn't end with a comma, value continues
            cont_field = field

            if sline.endswith(","):
                # trim the comma and close the field
                sline = sline[:-1]
                field = None

            if defn[cont_field].endswith('"') and sline.startswith('"'):
                # String concatenation; drop the "middle" quotes.
                defn[cont_field] = defn[cont_field][:-1] + sline[1:]
            else:
                # Normal continuation, just smoosh the values together?
                defn[cont_field] += sline
        else:
            assert sline.startswith("."), f"defn line {sline!r} doesn't start with '.'"
            tokens = sline.split("=", maxsplit=1)
            assert len(tokens) == 2
            field = tokens[0].strip()[1:]
            value = tokens[1].strip()
            if value.endswith(","):
                comma_terminated = True
                value = value[:-1]
            else:
                comma_terminated = False
            defn[field] = value
            if comma_terminated:
                field = None

    return defns


def split_args(args):
    ret = []
    if args:
        ret = args.split(",")
    return ret


def argspec_to_qapi(argspec):
    name, argtype = argspec.split(":")
    optional = argtype.endswith("?")
    if optional:
        argtype = argtype[:-1]

    if argtype.startswith("-"):
        # These are always "options", not types; force to bool.
        # They're also always optional!
        argtype = "b"
        optional = True

    json_type = arg_type_map[argtype]

    if optional:
        name = "*" + name

    return name, json_type


def hx_to_qapi(defn, info):
    name = defn['name']

    qapi_defn = { 'command': name }

    args = split_args(defn['args_type'])
    data = {}
    for arg in args:
        argname, json_type = argspec_to_qapi(arg)
        data[argname] = json_type
    if data:
        qapi_defn['data'] = data

    if 'if' in defn:
        qapi_defn['if'] = defn['if']

    # Hardcoded: the only definition in .hx files that cannot be
    # correctly determined - and exists over a very long period of time
    # - is device_add. To avoid needing dozens of addendum files that
    # only define this one command, simply hardcode it here.
    if name == 'device_add':
        qapi_defn['data'] = {
            'driver': 'str',
            'id': 'str',
        }
        qapi_defn['gen'] = False

    return QAPIExpression(qapi_defn, info, None)


def hxparse(filename, unqapified_only: bool = False):
    defns = parse_hxfile(filename)

    # For very old QEMU sources, filter out HMP commands:
    qmp_defns = [
        defn for defn in defns if 'mhandler.cmd' not in defn
    ]

    # Trim off the extra string quote confetti
    for defn in qmp_defns:
        for key, value in defn.items():
            if value.startswith('"') and value.endswith('"'):
                defn[key] = value[1:-1]

    # Very old QEMU has aliases; process those here.
    for defn in qmp_defns:
        if '|' in defn['name']:
            names = defn['name'].split('|')
            defn['name'] = names.pop()
            for name in names:
                new_defn = defn.copy()
                new_defn['name'] = name
                qmp_defns.append(new_defn)

    def _handler(defn):
        handler = defn.get('mhandler.cmd_new')
        if not handler:
            handler = defn['mhandler.cmd_async']
        return handler

    if unqapified_only:
        qmp_defns = [defn for defn in qmp_defns if not _handler(defn).startswith("qmp_marshal_")]

    # Mock info object
    info = QAPISourceInfo(str(filename), None)
    return [hx_to_qapi(defn, info) for defn in qmp_defns]


if __name__ == '__main__':
    filename = sys.argv[1]
    defns = parse_hxfile(filename)
    for defn in defns:
        qapi_expr = hx_to_qapi(defn)
        print(json.dumps(qapi_expr, indent=4))
        print("")
