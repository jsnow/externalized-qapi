#!/usr/bin/env python3
import json
import sys
from typing import Union, Dict, Union, Set


def print_header(header: str, level: int, nl: bool = True) -> None:
    syntax = {
        1: "#",
        2: "*",
        3: "=",
        4: "-",
        5: "^",
        6: '"',
    }
    hr = syntax[level] * len(header)
    if level <= 2:
        print(hr)
    print(header)
    print(hr)
    if nl:
        print("")


def get_scalar_type(defn) -> Union[str, set]:
    """
    Return a simple string type name for a JSON Schema object.

    Returns:
      - "boolean", "integer", "null", "number" or "string" for
        actual scalars.
      - "any" if there is no type specified.
      - "<cyclical reference>" for any $ref object.
      - "<discriminator-value>" for the tag member type *in a specified branch*.
        This should not be shown in diff output under normal circumstances,
        but is used for assertions guarding assumptions about the input.
      - a set of strings to represent a string enum.
    """
    assert isinstance(defn, dict)

    if "$ref" in defn:
        # This isn't strictly true for JSON Schema in general, it's an
        # assumption about how the compiled QAPI schema was constructed.
        return "<cyclical reference>"

    if "const" in defn:
        # Same comment as above for $ref.
        return "<discriminator-value>"

    if "type" not in defn:
        return "any"

    if defn["type"] in ("boolean", "integer", "null", "number"):
        return defn["type"]

    if defn["type"] == "string":
        if defn.get("enum"):
            return set(defn["enum"])
        return "string"

    # Non-scalar type; must be array/object.
    return None


def summarize_type(defn, path: str) -> Dict[str, Union[str, Set[str]]]:
    """
    Return a dictionary of json-path: scalar-type pairs.

    This is useful for generating differences between two objects with
    potentially nested values that need to be compared.

    Can return k:v pairs in the following formats:

    "field_name": field_type
      - where field_type is one of: string, integer, number, boolean, null,
                                    array, object, alternate
      - enums are represented by a set of enum values instead of "enum".

    "field_name[]": element_type
      - to denote the type of each element within an array

    "field_name<alternate_name>": alt_type
      - to denote the type of this field for a specific alternate type

    "field_name<tag=value>": "object"
      - to denote a path underneath a union; however this is never used
        without a leaf value - a branch type is *always* object.
    """

    # Take a look at the docs in compile.py to understand what type of
    # objects you'll see as input in this function - they're all JSON
    # Schema objects.

    # This is an alternate - an anonymous union.
    if "oneOf" in defn and 'type' not in defn:
        assert 'x-discriminator' not in defn
        ret = {path: "alternate"}
        for branch in defn["oneOf"]:
            subpath = f"{path}<{branch['x-qemu-branch-name']}>"
            ret.update(summarize_type(branch, subpath))
        return ret

    # This handles the "any" type, as well as
    # boolean/null/integer/number/string/enum.
    if (scalar := get_scalar_type(defn)) is not None:
        return {path: scalar}

    if defn["type"] == "array":
        assert "items" in defn
        ret = {path: "array"}
        subpath = f"{path}[]"
        ret.update(summarize_type(defn["items"], subpath))
        return ret

    if defn["type"] == "object":
        ret = {path: "object"}

        # Normal, god-fearing object properties:
        for fieldname, fielddef in defn.get("properties", {}).items():
            subpath = f"{path}.{fieldname}"
            ret.update(summarize_type(fielddef, subpath))

            if fieldname not in defn.get("required", []):
                # This is an optional field, annotate it as such.
                # The syntax is arbitrary.
                subtype = ret[subpath]
                if isinstance(subtype, set):
                    subtype = "enum"
                ret[subpath] = f"Optional<{subtype}>"

        # Branches :(
        assert bool('oneOf' in defn) == bool('x-discriminator' in defn)
        tag_member = defn.get('x-discriminator')
        for branch in defn.get('oneOf', []):
            assert branch['type'] == 'object'
            tag_value = branch['properties'][tag_member]['const']
            subpath = f"{path}<{tag_member}={tag_value}>"
            branch_ret = summarize_type(branch, subpath)

            # Delete the branch type itself from the summary; It's
            # always going to be "object" and isn't that useful to show
            # because it can't ever be anything else.
            assert branch_ret[subpath] == "object"
            del branch_ret[subpath]

            # Delete the discriminator from the branch summary, too:
            # we've already covered it in the base object and do not
            # need to list it N additional times.
            assert branch_ret[f"{subpath}.{tag_member}"] == "<discriminator-value>"
            del branch_ret[f"{subpath}.{tag_member}"]

            # Merge branch into the parent object.
            ret.update(branch_ret)

        return ret

    assert False, f"unhandled schema object {defn}"


def compare_summaries(summary_a, summary_b, removed: bool = True):
    if removed:
        token = "--"
        sort_prio = 1
        summary = summary_a
        ref_summary = summary_b
    else:
        token = "++"
        sort_prio = 2
        summary = summary_b
        ref_summary = summary_a

    ret = []

    for key, value in summary.items():
        if key not in ref_summary or value != ref_summary[key]:
            ref_value = ref_summary.get(key)

            # If both types are enums (represented as sets),
            # detail which values have been (removed/added).
            if isinstance(value, set) and isinstance(ref_value, set):
                if diff_values := value - ref_value:
                    ret.append((key, 0, "enum", "··", False))
                    #print(f"    ·· {key}: enum")
                    for enum_val in diff_values:
                        ret.append((key, sort_prio, enum_val, token, True))
                        #print(f"    {token}     {enum_val!r}")
            else:
                # Otherwise, just print out the (removed/added/modified) key/type.
                # Just summarize it as an enum if it happens to be one.
                if isinstance(value, set):
                    value = "enum"
                ret.append((key, sort_prio, value, token, False))
                #print(f"    {token} {key}: {value}")

    return ret


def compare_definitions(schema_a, schema_b, kind: str = "commands"):
    assert kind in ("commands", "events")
    key = f"x-qemu-{kind}"

    defns_a = set(schema_a[key]["enum"])
    defns_b = set(schema_b[key]["enum"])

    print_header(kind, level=2)

    new_defns = defns_b - defns_a
    if new_defns:
        print_header("Added", level=3, nl=False)
        for defn in sorted(list(new_defns)):
            print(defn)
        print("")

    del_defns = defns_a - defns_b
    if del_defns:
        print_header("Removed", level=3, nl=False)
        for defn in sorted(list(del_defns)):
            print(defn)
        print("")

    common_defns = defns_a & defns_b
    modified_defns = []
    for defn in sorted(list(common_defns)):
        def_a = schema_a["$defs"][defn]
        def_b = schema_b["$defs"][defn]
        if def_a != def_b:
            modified_defns.append(defn)

    if modified_defns:
        print_header("Modified", level=3, nl=False)
        for defn in modified_defns:
            def_a = schema_a["$defs"][defn]
            def_b = schema_b["$defs"][defn]
            # Which things changed about it?
            # (arguments, returns, features, if)
            what = []
            for key, val in def_a.items():
                if def_a[key] != def_b.get(key):
                    what.append(key)
            what = [item.removeprefix("x-qemu-") for item in what]
            print(f"{defn} ({', '.join(what)})")

            if kind == "commands":
                summary_a = summarize_type(def_a["x-qemu-arguments"], "arguments")
                summary_a.update(
                    summarize_type(
                        def_a.get("x-qemu-returns", {"type": "object"}), "returns"
                    )
                )
                summary_b = summarize_type(def_b["x-qemu-arguments"], "arguments")
                summary_b.update(
                    summarize_type(
                        def_b.get("x-qemu-returns", {"type": "object"}), "returns"
                    )
                )
            elif kind == "events":
                summary_a = summarize_type(def_a["x-qemu-members"], "members")
                summary_b = summarize_type(def_b["x-qemu-members"], "members")

            diff_tuples = []
            ret = compare_summaries(summary_a, summary_b, removed=True)
            diff_tuples.extend(ret)

            ret = compare_summaries(summary_a, summary_b, removed=False)
            diff_tuples.extend(ret)

            # Sort based on key name; break ties on the token (··, --, ++)
            diff_tuples = sorted(set(diff_tuples))
            for key, _, value, token, is_enum in diff_tuples:
                if is_enum:
                    print(f"    {token}     {value!r}")
                else:
                    print(f"    {token} {key}: {value}")

        print("")


def compare(file_a, file_b):
    with open(file_a, "r") as file:
        schema_a = json.load(file)

    with open(file_b, "r") as file:
        schema_b = json.load(file)

    ver_a = "v" + ".".join(str(x) for x in schema_a["x-qemu-version"])
    ver_b = "v" + ".".join(str(x) for x in schema_b["x-qemu-version"])
    print_header(f"{ver_a} ==> {ver_b}", level=1)

    print(
        "Note: The following diff output may or may not present differences to \n"
        "'alternate' fields if the QAPI alternate definitions had their branch \n"
        "names changed between versions. These names are not part of the QMP \n"
        "wire protocol, but otherwise help to differentiate branches of the \n"
        "anonymous union.\n"
    )

    compare_definitions(schema_a, schema_b, "commands")
    compare_definitions(schema_a, schema_b, "events")


def main():
    file_a = sys.argv[1]
    file_b = sys.argv[2]

    compare(file_a, file_b)


if __name__ == "__main__":
    main()
